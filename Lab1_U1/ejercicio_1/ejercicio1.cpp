/*
  * g++ ejercicio1.cpp -o ejercicio1
 */

#include <iostream>
using namespace std;
#include "Cuadrado.h"

// main
int main(int argc, char *argv[])
{
  int cantidad;
  int number;
  int sumaCuadrado = 0;

  // se le pide al usuario ingresar tamaño del arreglo
  cout << "Cantidad de numeros que quiera ingresar:"<< endl;
  cin >> cantidad;
  int numlist[cantidad];

  // ciclo en donde se le piden numeros al usuario
  for (int i = 0; i < cantidad; i++){
    cout << "Ingrese un numero: " << endl;
    cin >> number;
    // se genera el objeto cuadrado para cada numero
    Cuadrado cuadrado = Cuadrado();
    // se calcula el cuadrado del numero
    cuadrado.set_numCuadrado(number*number);
    numlist[i] = cuadrado.get_numCuadrado();
  }
  // salida del resultado
  cout << "******RESULTADO*******" << endl;
  for (const auto& i : numlist){
    sumaCuadrado = sumaCuadrado + i;
  }
  cout << "La suma de los cuadrados de numeros ingresados es: " << sumaCuadrado << endl;
  return 0;
}
