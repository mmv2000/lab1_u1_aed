# Ejercicio 1
Se pide que este programa llene un arreglo unidimensional de números enteros y luego obtenga como resultado la suma del 
cuadrado de los números ingresados.

# Como funciona
El programa funciona de acuerdo al numero de datos ingresados, en este caso numeros enteros, los cuales son pedidos
al usuario que los ingrese. Luego estos numeros se multiplican por sí mismos (calculo del numero al cuadrado)
para ser agregados a una lista, la cual posteriormente sera iterada en cada posicion donde se sumaran los cuadrados de
los numeros y se mostraran por pantalla.

# Para ejecutar
Se debe abrir la terminal e ingresar al directorio donde se guardó el ejercicio con sus archivos .cpp y .h
repectivamente. Luego ejecutar el comando make para su compilación y luego ejecutar el comando ./ejercicio1
para iniciar el programa.

# Construido con 

- Ubuntu: sistema operativo

- C++: lenguaje de programación

- iostream: libreria

- Atom: editor de texto

# Versiones

- Ubuntu 20.04 LTS

- Atom 1.46.0

# Autor

- Martín Muñoz Vera 
