#include <list>
#include <iostream>
using namespace std;

#ifndef CUADRADO_H
#define CUADRADO_H

class Cuadrado {

  private:
    int numCuadrado;

  public:
    // constructores
    Cuadrado();
    Cuadrado(int num_cuadrado);

    // metodos set y get
    int get_numCuadrado();
    void set_numCuadrado(int numCuadrado);
};
#endif
