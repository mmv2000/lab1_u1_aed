#include <iostream>
#include <list>
using namespace std;

#ifndef DATOS_H
#define DATOS_H

class Datos {
  private:
    int numMinuscula;
    int numMayuscula;

  public:
    Datos();
    Datos(int numMinuscula, int numMayuscula);

    int get_numMinuscula();
    int get_numMayuscula();
    void set_numMinuscula(int numMinuscula);
    void set_numMayuscula(int numMayuscula);
};
#endif
