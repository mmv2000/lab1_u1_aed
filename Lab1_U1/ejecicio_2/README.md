# Ejercicio 2
Se pide que este programa lea N frases en un arreglo de caracteres y determine el número de minúsculas
y mayúsculas que hay en cada una de ellas.

# Como funciona
El programa inicia con la consulta al usuario sobre la cantidad de frases que se desea ingresar, las cuales posteriormente
serian las variables a analizar, contando el numero de letras minúsculas y mayúsculas presente en cada frase. Finalmente 
mostrará la cantidad en cada frase. 

# Para ejecutar
Se debe abrir la terminal e ingresar al directorio donde se guardó el ejercicio con sus archivos .cpp y .h
repectivamente. Luego ejecutar el comando make para su compilación y luego ejecutar el comando ./ejercicio2
para iniciar el programa.

# Construido con 

- Ubuntu: sistema operativo

- C++: lenguaje de programación

- Atom: editor de texto

# Versiones

- Ubuntu 20.04 LTS

- Atom 1.46.0

# Autor

- Martín Muñoz Vera 

