/*
  * g++ Datos.cpp -o Datos
 */

 #include <iostream>
 #include <list>
 #include "Datos.h"
using namespace std;

Datos::Datos() {
  int numMinuscula = 0;
  int numMayuscula = 0;
}

Datos::Datos (int numMinuscula, int numMayuscula) {
  this-> numMinuscula = numMinuscula;
  this-> numMayuscula = numMayuscula;
}

int Datos::get_numMinuscula() {
  return this-> numMinuscula;
}

int Datos::get_numMayuscula() {
  return this-> numMayuscula;
}

void Datos::set_numMinuscula(int numMinuscula) {
  this-> numMinuscula = numMinuscula;
}

void Datos::set_numMayuscula(int numMayuscula) {
  this-> numMayuscula = numMayuscula;
}
