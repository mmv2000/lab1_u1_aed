/*
  * g++ ejercicio2.cpp -o ejercicio2
 */

#include <iostream>
#include <list>
//#include <ctype>
using namespace std;
#include "Datos.h"

int main(int argc, char *argv[]) {
  int size;
  string chain;

  cout << "Ingrese la cantidad de frases que desea evaluar:" << endl;
  cin >> size;
  string frases[size];

  for (int i = 0; i < size; i++){
    cout << "Escriba la frase" << endl;
    cin >> chain;
    cout << "FRASE: " << chain << endl;
  }
  cout << frases << endl;
  return 0;
}
