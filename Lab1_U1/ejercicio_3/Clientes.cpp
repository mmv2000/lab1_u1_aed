#include <iostream>
#include <list>
#include "Clientes.h"
using namespace std;

Clientes::Clientes() {
  string nombre = "\0";
  string telefono = "\0";
  int saldo = 0;
  bool moroso = NULL;
  list<Clientes> clientes;
}

Clientes::Clientes(string nombre, string telefono, int saldo, bool moroso) {
  this-> nombre = nombre;
  this-> telefono = telefono;
  this-> saldo = saldo;
  this-> moroso = moroso;
  list<Clientes> clientes;
}

list <Clientes> Clientes::get_clientes() {
  return this-> clientes;
}

string Clientes::get_nombre() {
  return this-> nombre;
}

string Clientes::get_telefono() {
  return this-> telefono;
}

int Clientes::get_saldo() {
  return this-> saldo;
}

bool Clientes::get_moroso() {
  return this-> moroso;
}

void Clientes::set_nombre(string nombre) {
  this-> nombre = nombre;
}

void Clientes::set_telefono(string telefono) {
  this-> telefono = telefono;
}

void Clientes::set_saldo(int saldo) {
  this-> saldo = saldo;
}

void Clientes::set_moroso(bool moroso) {
  this-> moroso = moroso;
}

void Clientes::add_clientes(Clientes clientes) {
  this-> clientes.push_back(clientes);
}
