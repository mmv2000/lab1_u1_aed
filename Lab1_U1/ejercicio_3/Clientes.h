#include <iostream>
#include <list>
using namespace std;

#ifndef CLIENTES_H
#define CLIENTES_H

class Clientes {
  private:
    string nombre;
    string telefono;
    int saldo;
    bool moroso;
    list<Clientes> clientes;

  public:
    Clientes();
    Clientes(string nombre, string telefono, int saldo, bool moroso);

    string get_nombre();
    string get_telefono();
    int get_saldo();
    bool get_moroso();
    list<Clientes> get_clientes();
    void set_nombre(string nombre);
    void set_telefono(string telefono);
    void set_saldo(int saldo);
    void set_moroso(bool moroso);
    void add_clientes(Clientes clientes);
};
#endif
