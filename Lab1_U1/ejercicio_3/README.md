# Ejercicio3
Una empresa registra para cada uno de sus N clientes los siguientes datos:
Nombre (cadena de caracteres)
Teléfono (cadena de caracteres)
Saldo (entero)
Moroso (booleano)

Se pide escribir un programa que permita el ingreso de los datos de los clientes y luego permita imprimir sus
datos indicando su estado (morosos o no). Utilizando un     arreglo de clases en su solución.

# Como funciona
El programa deberia iniciar con la consulta al usuario de cuantos clientes desea ingresar para determinar el tamaño del arreglo.
Luego se ingresan los respectivos datos de los clientes: nombre, telefono, saldo y estado (moroso o no), para luego imprimirlos en pantalla

# Consejos extra 
Para este proyecto se ocupa la librería Gtk donde se importa como si no la tiene debe instalarla con el siguiente codigo:
 sudo apt-get install python3-gi

# Para ejecutar
Se debe abrir la terminal e ingresar al directorio donde se guardó el ejercicio con sus archivos .cpp y .h
repectivamente. Luego ejecutar el comando make para su compilación y luego ejecutar el comando ./ejercicio3
para iniciar el programa.

# Construido con 

- Ubuntu: sistema operativo

- C++: lenguaje de programación

- Atom: editor de texto

# Versiones

- Ubuntu 20.04 LTS

- Atom 1.46.0

# Autor

- Martín Muñoz Vera
