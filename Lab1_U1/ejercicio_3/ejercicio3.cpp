/*
  * g++ ejercicio3.cpp -o ejercicio3
 */

#include <iostream>
#include <list>
using namespace std;
#include "Clientes.h"

int main(int argc, char *argv[]) {
  string line;
  int tam;
  bool moroso;
  list<Clientes> clientes;

  //cout << "Ingrese numero de clientes que quiera almacenar:" << endl;
  //cin >> tam;
  Clientes c1 = Clientes();
  cout << "Nombre cliente: " << endl;
  getline(cin, line);
  c1.set_nombre(line);
  //  c1.get_nombre();

  cout << "telefono: " << endl;
  getline(cin, line);
  c1.set_telefono(line);
  //  c1.get_telefono();

  cout << "Saldo cliente: " << endl;
  getline(cin, line);
  c1.set_saldo(stoi(line));
//    c1.get_saldo();

//    clientes.push_back(c1);

  cout << "Moroso 0 para no, 1 para si: ";
  cin >> moroso;
  c1.set_moroso(moroso);

  cout << "Nombre: " << c1.get_nombre() << endl;
  cout << "Telefono: " << c1.get_telefono() << endl;
  cout << "Saldo: " << c1.get_saldo() << endl;
  cout << "Moroso: " << c1.get_moroso() << endl;
  return 0;
}
